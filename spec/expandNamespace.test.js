const proxyquire = require( 'proxyquire' );

const sinon = require( 'sinon' );
const chai = require( 'chai' );

describe( 'The ./lib/expandNamespaces function', ()=>{

  it( 'should keep paths without namespaces unchanged', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub()
    } );

    const expected = './somePath';
    const actual   = expandNamespaces( expected );
    var expect = chai.expect;

    expect( actual ).to.equal( expected );

  } );

  it( 'for null paths should return null', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub()
    } );

    const expected = null;
    const actual   = expandNamespaces( expected );
    var expect = chai.expect;

    expect( actual ).to.equal( expected );

  } );

  it( ' empty path should return empty path ', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub()
    } );

    const expected = "";
    const actual   = expandNamespaces( expected );
    var expect = chai.expect;

    expect( actual ).to.equal( expected );

  } );

  it( ' valid paths should return the relative path ', ()=>{

    const expandNamespaces = proxyquire( '../lib/expandNamespace', {
      './loadNamespaces': sinon.stub()
    } );

    const param = "<loadNamespaces>";
    const expected = "./loadNamespaces";
    const actual   = expandNamespaces( param, "asd" );
    var expect = chai.expect;

    expect( actual ).to.equal( expected );

  } );

} );
